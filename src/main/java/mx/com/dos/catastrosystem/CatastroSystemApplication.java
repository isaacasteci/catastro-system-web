package mx.com.dos.catastrosystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CatastroSystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(CatastroSystemApplication.class, args);
	}

}
